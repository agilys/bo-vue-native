module.exports = {
  devServer: {
    clientLogLevel: 'info', // bug in Vue cli 3.7 https://github.com/vuejs/vue-cli/issues/4017
  },
};
