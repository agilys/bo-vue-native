import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home.vue';
import Callback from './components/auth0/Callback.vue';
import CallApi from './pages/CallApi.vue';
import About from './pages/About.vue';
import Auth from './services/auth/auth';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'About',
      component: About,
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback,
    },
    {
      path: '/callApi',
      name: 'callApi',
      component: CallApi,
    },
  ],
});

// eslint-disable-next-line consistent-return
router.beforeEach((to, from, next) => {
  if (to.path === '/callback' || Auth.isAuthenticated()) {
    return next();
  }
  Auth.login({ target: to.path });
});

export default router;
